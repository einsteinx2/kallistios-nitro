# Stage 1
FROM alpine:latest as build
MAINTAINER DreamSDK <team@dreamsdk.org>

# Installing prerequisites
RUN apk --update add --no-cache \
	build-base \
	patch \
	bash \
	texinfo \
	libjpeg-turbo-dev \
	libpng-dev \
	curl \
	wget \
	git \
	python \
	subversion \
	&& apk --update add --no-cache libelf-dev --repository=http://dl-cdn.alpinelinux.org/alpine/v3.9/main \
	&& rm -rf /var/cache/apk/*

# Retrieving dc-chain
RUN mkdir -p /opt/toolchains/dc \
	&& git clone https://gitlab.com/simulant/community/kallistios-nitro.git /opt/toolchains/dc/kos

# Setting up gcc-9.2.0
RUN sed -i 's/4.7.4/9.2.0/' /opt/toolchains/dc/kos/utils/dc-chain/Makefile
RUN sed -i 's/m4-single-only,m4-nofpu,m4/m4-single-only,m4/' /opt/toolchains/dc/kos/utils/dc-chain/Makefile
RUN sed -i 's/gcc-$GCC_VER.tar.bz2/gcc-$GCC_VER.tar.xz/' /opt/toolchains/dc/kos/utils/dc-chain/download.sh
RUN sed -i 's/gcc-$GCC_VER.tar.bz2/gcc-$GCC_VER.tar.xz/' /opt/toolchains/dc/kos/utils/dc-chain/unpack.sh

# Setting up newlib-2.2.0
RUN sed -i 's/2.0.0/2.2.0/' /opt/toolchains/dc/kos/utils/dc-chain/Makefile
	
# Preparing toolchains
RUN cd /opt/toolchains/dc/kos/utils/dc-chain && ./download.sh --no-deps && ./unpack.sh --no-deps \
	&& cd /opt/toolchains/dc/kos/utils/dc-chain/gcc-9.2.0 && ./contrib/download_prerequisites && cd ..

# Making toolchains
RUN cd /opt/toolchains/dc/kos/utils/dc-chain && make && make gdb && rm -rf /opt/toolchains/dc/kos

# Stage 2
FROM scratch
COPY --from=build / /
